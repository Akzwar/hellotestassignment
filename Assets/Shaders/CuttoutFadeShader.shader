﻿Shader "Custom/CuttoutFadeShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_AlphaCutoff( "AlphaCutoff", Range( 0, 1 ) ) = 0.5
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows alpha:fade

		#pragma target 3.0

		sampler2D _MainTex;
		float _AlphaCutoff;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed4 main_tex_color = tex2D( _MainTex, IN.uv_MainTex );
			fixed4 c = main_tex_color * _Color;
			o.Albedo = c.rgb;

			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;

			o.Alpha = _Color.a;
			if( main_tex_color.a < 1 - _AlphaCutoff )
				o.Alpha = 0;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
