﻿using UnityEngine;

public class ManipulatorController : MonoBehaviour
{
    public MeshRenderer ManipulationRenderer;
	
	void Update ()
    {
        if( ManipulationRenderer == null )
            return;

        Vector2 viewport_position = Camera.main.WorldToViewportPoint( transform.position );

        ManipulationRenderer.material.SetFloat( "_AlphaCutoff", viewport_position.x / 0.9f );

        Color color = ManipulationRenderer.material.color;
        color.a = transform.parent.eulerAngles.z % 60.0f / 60.0f;

        ManipulationRenderer.material.color = color;
	}
}
