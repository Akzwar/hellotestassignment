﻿Shader "Custom/YShader" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_UVTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader {
		Pass {
		
			CGPROGRAM
				#pragma vertex vert_img
				#pragma fragment frag

				#include "UnityCG.cginc"

				uniform sampler2D _MainTex;
				uniform sampler2D _UVTex;

				fixed4 frag(v2f_img texCoord) : SV_Target {
					
					float y = tex2D(_MainTex, texCoord.uv).r;		// using GL_LUMINACE so each channel is the same
					float u = tex2D(_UVTex, texCoord.uv).a - 0.5;
					float v = tex2D(_UVTex, texCoord.uv).r - 0.5;

					//float r = y + 1.13983*v;
					float r = y + 1.370705*v;
					//float g = y - 0.39465*u - 0.58060*v;
					float g = y - 0.337633*u - 0.698001*v;
					//float b = y + 2.03211*u;
					float b = y + 1.732446*u;
					return float4(b,g,r,1.0);
					//return float4(y,y,y,1.0);
				}
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
